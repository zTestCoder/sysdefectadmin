import os
import config

from flask import Flask, Blueprint

from plugins import init_plugs
from common import register_api
from common.utils.upload import photos
from common.flask_uploads import configure_uploads

# 引入路由
from application.view import init_view

# 引入接口（蓝图在本页项目初始化时建立）
from application.system import register_sys_api
from application.users import register_users_api
from application.rights import register_rights_api
from application.defect import register_defect_api
from application.projects import register_projects_api

api_bp: Blueprint = Blueprint('api', __name__, url_prefix='/api/v1')
# 接口蓝图：注册登录接口
register_sys_api(api_bp)
register_users_api(api_bp)
register_rights_api(api_bp)
register_defect_api(api_bp)
register_projects_api(api_bp)

app = Flask('systestadmin')


def init_api(app: Flask) -> None:
    app.register_blueprint(api_bp)


def create_app() -> Flask:
    # 引入数据库配置
    app.config.from_object(config)

    # 注册插件
    init_plugs(app)

    # 注册路由
    init_view(app)

    # 注册接口
    init_api(app)

    # 文件上传
    configure_uploads(app, photos)

    if os.environ.get('WERKZEUG_RUN_MAIN') == 'true':
        logo()

    return app


def logo():
    print(
        r'''
 __     __            _____                     _______        _
 \ \   / /           |  __ \                   |__   __|      | |
  \ \_/ /   _ _ __   | |__) |___  _ __   __ _     | | ___  ___| |
   \   / | | | '_ \  |  _  // _ \| '_ \ / _` |    | |/ _ \/ __| __|
    | || |_| | | | | | | \ \ (_) | | | | (_| |    | |  __/\__ \ |
    |_| \__,_|_| |_| |_|  \_\___/|_| |_|\__, |    |_|\___||___/\__|
                                        __/  |
                                        |___/
    '''
    )


create_app()
