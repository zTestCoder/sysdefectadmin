import re
from flask import Flask
from datetime import datetime

date_str = re.compile(r'\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d$')


def add_data(data_list, obj):
    from plugins import db

    for _data in data_list:
        dept = obj()
        for key, value in _data.items():
            if isinstance(value, str) and date_str.match(value):
                value = datetime.strptime(value, "%Y-%m-%d %H:%M:%S")
            setattr(dept, key, value)
        db.session.add(dept)
    db.session.commit()


def register_script(app: Flask):
    @app.cli.command()
    def init_db():
        import os
        import json

        path = os.path.dirname(os.path.abspath(__name__))
        path = os.path.join(path, 'static', 'data')
        # 初始化数据：部门数据
        cp_dept_data_list = json.loads(open(os.path.join(path, 'cp_dept.json'), encoding='utf-8').read())
        # 初始化数据：注册用户
        cp_user_data_list = json.loads(open(os.path.join(path, 'cp_user.json'), encoding='utf-8').read())
        # 初始化数据：角色数据
        rt_role_data_list = json.loads(open(os.path.join(path, 'rt_role.json'), encoding='utf-8').read())
        # 初始化数据：菜单数据
        rt_power_data_list = json.loads(open(os.path.join(path, 'rt_power.json'), encoding='utf-8').read())
        # 初始化数据：角色管理
        rt_user_role_data_list = json.loads(open(os.path.join(path, 'rt_user_role.json'), encoding='utf-8').read())
        # 初始化数据：权限管理
        rt_role_power_data_list = json.loads(open(os.path.join(path, 'rt_role_power.json'), encoding='utf-8').read())

        # 初始化数据：上传图片
        file_photo_data_list = json.loads(open(os.path.join(path, 'file_photo.json'), encoding='utf-8').read())

        """数据库初始化"""

        # 创建化部门数据
        from models import DepartmentModel

        add_data(cp_dept_data_list, DepartmentModel)

        # 管理员用户
        from models import UserModel

        add_data(cp_user_data_list, UserModel)

        # 初始化角色表
        from models import RoleModel

        add_data(rt_role_data_list, RoleModel)

        # 初始化权限表数据
        from models import RightModel

        add_data(rt_power_data_list, RightModel)

        # 用户角色表
        from plugins import db

        for data in rt_user_role_data_list:
            db.session.execute('insert into rt_user_role VALUES (%s, %s, %s);' % tuple(data))
        db.session.commit()

        # 角色权限关系表
        from plugins import db

        for data in rt_role_power_data_list:
            db.session.execute('insert into rt_role_power VALUES (%s, %s, %s);' % tuple(data))
        db.session.commit()

        # 图片数据
        from models import PhotoModel

        add_data(file_photo_data_list, PhotoModel)

    @app.cli.command()
    def turn():
        """清空数据库"""
        from plugins import db

        db.drop_all()
        db.create_all()
