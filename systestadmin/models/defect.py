from datetime import datetime

from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash

from plugins import db


class DefectModel(db.Model):
    __tablename__ = 'cp_defect'
    id = db.Column(db.Integer, primary_key=True, comment="缺陷ID")
    space_id = db.Column(db.String(32), comment="奋进号编号")
    bug_title = db.Column(db.String(32), nullable=True, comment="缺陷标题")
    bug_type = db.Column(db.String(2), default='1', comment="缺陷类型(1-缺陷)")
    bug_level = db.Column(db.String(2), default='2', comment="严重级别(1-小缺陷,2-一般,3-严重,4-致命)")
    bug_status = db.Column(db.String(2), default='1', comment="缺陷状态(1-新建,2-已关闭,3-已否决,4-暂时保留)")
    fix_num = db.Column(db.Integer, nullable=True, comment="修复次数")
    hit_run = db.Column(db.Boolean, nullable=True, comment="是否逃逸(1是,0否)")
    bug_design_type = db.Column(db.String(2), comment='设计缺陷(1-设计与需求不符,2-设计文档缺失,3-设计考虑不全,4-设计逻辑错误,5-设计优化)')
    bug_require_type = db.Column(db.String(2), comment='需求缺陷(1-需求优化,2-需求缺失,3-需求与逻辑不符,4-需求模板规范不符)')
    bug_require_stage = db.Column(db.String(2), comment='需求缺陷发现阶段(1-需求阶段,2-设计阶段,3-开发阶段,4-测试阶段)')
    project_name = db.Column(db.String(64), comment='项目名称')
    system_name = db.Column(db.String(64), comment='系统名称')
    programmer = db.Column(db.String(32), comment='开发姓名')
    is_own = db.Column(db.String(32), comment='是否我方开发')
    tester = db.Column(db.String(32), comment='测试姓名')
    bug_desc = db.Column(db.Text, comment="缺陷描述")

    create_at = db.Column(db.DateTime, default=datetime.now, comment='创建时间')
    update_at = db.Column(db.DateTime, default=datetime.now, onupdate=datetime.now, comment='更新时间')
