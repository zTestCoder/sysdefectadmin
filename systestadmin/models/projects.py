from datetime import datetime

from plugins import db


class ProjectsModel(db.Model):
    __tablename__ = "cp_projects"
    id = db.Column(db.Integer, primary_key=True, comment="ID")
    sysetm_name = db.Column(db.String(128), nullable=False, comment="系统名称")
    project_name = db.Column(db.String(128), nullable=False, comment="项目名称")
    project_type = db.Column(db.String(128), nullable=False, comment="项目类型")
    project_desc = db.Column(db.String(1024), nullable=False, comment="项目描述")
    project_status = db.Column(db.String(128), nullable=False, comment="项目状态")
    project_owner = db.Column(db.String(128), nullable=False, comment="项目经理")
    project_team = db.Column(db.Text, nullable=False, comment="项目成员")
    project_creator = db.Column(db.String(128), nullable=False, comment="创建人")
    created_at = db.Column(db.DateTime, default=datetime.now, comment="创建时间")
    updated_at = db.Column(db.DateTime, default=datetime.now, onupdate=datetime.now, comment="更新时间")
