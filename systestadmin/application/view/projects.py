from flask import Blueprint, render_template

from plugins import db
from common.utils.rights import permission_required, view_logging_required

from models import ProjectsModel


projects_bp = Blueprint("project", __name__, url_prefix="/admin/projects")


# 项目管理
@projects_bp.route("/")
@view_logging_required
@permission_required("admin:projects:main")
def main():
    return render_template("admin/projects/project.html")
