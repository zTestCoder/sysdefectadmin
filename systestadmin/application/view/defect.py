from flask import Blueprint, render_template

from plugins import db
from common.utils.rights import permission_required, view_logging_required

from models import UserModel
from models import DefectModel
from models import DepartmentModel


defect_bp = Blueprint('defect', __name__, url_prefix='/admin/defect')


# 缺陷管理
@defect_bp.get('/')
@view_logging_required
@permission_required("admin:defect:main")
def main():
    return render_template('admin/defect/defect.html')


# 缺陷新增
@defect_bp.get('/add')
@view_logging_required
@permission_required("admin:defect:add")
def defect_add():
    # 搜索测试部门成员姓名
    depart_test_ids = db.session.query(DepartmentModel).filter(DepartmentModel.parent_id == '4').all()
    dept_test_users = db.session.query(UserModel).filter(UserModel.dept_id.in_([id.id for id in depart_test_ids])).all()
    dept_test_users = [id.realname for id in dept_test_users]
    # 搜索开发部门成员姓名
    depart_code_ids = db.session.query(DepartmentModel).filter(DepartmentModel.parent_id.in_(['5', '14'])).all()
    dept_code_users = db.session.query(UserModel).filter(UserModel.dept_id.in_([id.id for id in depart_code_ids])).all()
    dept_code_users = [id.realname for id in dept_code_users]
    return render_template('admin/defect/defect_add.html', dept_test_users=dept_test_users, dept_code_users=dept_code_users)


# 缺陷编辑
@defect_bp.get('/edit/<int:defect_id>')
@view_logging_required
@permission_required("admin:defect:edit")
def role_editor(defect_id):
    defect = DefectModel.query.filter_by(id=defect_id).first()
    # 搜索测试部门成员姓名
    depart_test_ids = db.session.query(DepartmentModel).filter(DepartmentModel.parent_id == '4').all()
    dept_test_users = db.session.query(UserModel).filter(UserModel.dept_id.in_([id.id for id in depart_test_ids])).all()
    dept_test_users = [id.realname for id in dept_test_users]
    # 搜索开发部门成员姓名
    depart_code_ids = db.session.query(DepartmentModel).filter(DepartmentModel.parent_id.in_(['5', '14'])).all()
    dept_code_users = db.session.query(UserModel).filter(UserModel.dept_id.in_([id.id for id in depart_code_ids])).all()
    dept_code_users = [id.realname for id in dept_code_users]
    return render_template('admin/defect/defect_edit.html', defect=defect, dept_test_users=dept_test_users, dept_code_users=dept_code_users)
