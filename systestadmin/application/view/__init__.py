from flask import Flask

from .roles import role_bp
from .index import index_bp
from .logs_view import logs_bp
from .defect import defect_bp
from .projects import projects_bp

from . import department
from . import passport
from . import rights
from . import users
from . import file


def init_view(app: Flask):
    # 注册蓝图
    app.register_blueprint(role_bp)
    app.register_blueprint(logs_bp)
    app.register_blueprint(index_bp)
    app.register_blueprint(defect_bp)
    app.register_blueprint(projects_bp)
