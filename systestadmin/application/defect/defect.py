import os
import uuid
import base64
from flask import request
from flask import current_app
from flask.views import MethodView
from common.utils.http import table_api, success_api, fail_api
from common.utils.upload import upload_one
from common.flask_uploads import UploadSet, IMAGES

from plugins import db

from models import PhotoModel
from models import DefectModel


photos = UploadSet('photos', IMAGES)


class DefectApi(MethodView):
    # 缺陷列表查询
    def get(self, _id):
        if not _id:
            page = request.args.get('page', default=1, type=int)
            limit = request.args.get('limit', default=0, type=int)
            bug_title = request.args.get('bug_title', default='', type=str)
            tester = request.args.get('tester', default='', type=str)

            filters = []
            if bug_title:
                filters.append(DefectModel.bug_title.like('%' + bug_title + '%'))
            if tester:
                filters.append(DefectModel.tester.like('%' + tester + '%'))

            paginate = DefectModel.query.filter(*filters).paginate(page=page, per_page=limit, error_out=False)

            return table_api(
                result={
                    'items': [
                        {
                            'id': item.id,
                            'bug_title': item.bug_title,
                            'bug_type': item.bug_type,
                            'bug_level': item.bug_level,
                            'bug_status': item.bug_status,
                            'fix_num': item.fix_num,
                            'hit_run': item.hit_run,
                            'bug_design_type': item.bug_design_type,
                            'bug_require_type': item.bug_require_type,
                            'bug_require_stage': item.bug_require_stage,
                            'project_name': item.project_name,
                            'programmer': item.programmer,
                            'tester': item.tester,
                            'create_at': str(item.create_at),
                        }
                        for item in paginate.items
                    ],
                    'total': paginate.total,
                },
                code=0,
            )

    # 缺陷新增
    def post(self):
        # todo 添加校验
        bug_title = request.json.get('bug_title', '')
        space_id = request.json.get('space_id', '')
        bug_type = request.json.get('bug_type', '1')
        bug_level = request.json.get('bug_level', '2')
        bug_status = request.json.get('bug_status', '')
        fix_num = request.json.get('fix_num', 0)
        hit_run = request.json.get('hit_run', 0)
        bug_design_type = request.json.get('bug_design_type', '')
        bug_require_type = request.json.get('bug_require_type', '')
        bug_require_stage = request.json.get('bug_require_stage', '')
        project_name = request.json.get('project_name', '')
        system_name = request.json.get('system_name', '')
        programmer = request.json.get('programmer', '')
        is_own = request.json.get('is_own', 1)
        tester = request.json.get('tester', '')
        bug_desc = request.json.get('bug_desc', '')

        # 从富文本框中提取图片数据
        if bug_desc:
            for img in bug_desc.split('" alt="">'):
                start_tag = '<img src="'
                start = img.find(start_tag)
                if start > -1:
                    image_data = img[start + len(start_tag):]
                    if 'data' in image_data and 'image' in image_data:
                        image_bytes = base64.b64decode(image_data.split(',')[1])
                        image_ext = image_data.split(':')[1].split(';')[0]  # 获取图片扩展名
                        image_name = str(uuid.uuid4()) + '.' + image_ext.split(r'/')[1]
                        upload_url = current_app.config.get("UPLOADED_PHOTOS_DEST")
                        image_path = os.path.join(upload_url, image_name)
                        # 将图片保存到指定路径
                        with open(image_path, 'wb') as f:
                            f.write(image_bytes)
                        size = os.path.getsize(image_path)
                        image_url = photos.url(image_name)
                        # 保存图片上传信息
                        photo = PhotoModel(name=image_name, href=image_url, mime=image_ext, size=size)
                        db.session.add(photo)
                        db.session.commit()
                        # 替换原字符串
                        bug_desc = bug_desc.replace(str(image_data), image_url)

        bug = DefectModel(
            bug_title=bug_title,
            space_id=space_id,
            bug_type=bug_type,
            bug_level=bug_level,
            bug_status=bug_status,
            fix_num=fix_num,
            hit_run=int(hit_run),
            bug_design_type=bug_design_type,
            bug_require_type=bug_require_type,
            bug_require_stage=bug_require_stage,
            project_name=project_name,
            system_name=system_name,
            programmer=programmer,
            is_own=is_own,
            tester=tester,
            bug_desc=bug_desc,
        )

        db.session.add(bug)
        db.session.commit()
        return success_api(message="成功")

    # 缺陷修改
    def put(self, _id):
        # todo 添加校验
        bug_title = request.json.get('bug_title', '')
        space_id = request.json.get('space_id', '')
        bug_type = request.json.get('bug_type', '1')
        bug_level = request.json.get('bug_level', '2')
        bug_status = request.json.get('bug_status', '')
        fix_num = request.json.get('fix_num', 0)
        hit_run = request.json.get('hit_run', 0)
        bug_design_type = request.json.get('bug_design_type', '')
        bug_require_type = request.json.get('bug_require_type', '')
        bug_require_stage = request.json.get('bug_require_stage', '')
        project_name = request.json.get('project_name', '')
        system_name = request.json.get('system_name', '')
        programmer = request.json.get('programmer', '')
        is_own = request.json.get('is_own', 1)
        tester = request.json.get('tester', '')
        bug_desc = request.json.get('bug_desc', '')

        bug_desc = bug_desc.replace('alt="undefined"', 'alt=""')
        # 从富文本框中提取图片数据
        if bug_desc:
            for img in bug_desc.split('" alt="">'):
                start_tag = '<img src="'
                start = img.find(start_tag)
                if start > -1:
                    image_data = img[start + len(start_tag):]
                    if 'data' in image_data and 'image' in image_data:
                        image_bytes = base64.b64decode(image_data.split(',')[1])
                        image_ext = image_data.split(':')[1].split(';')[0]  # 获取图片扩展名
                        image_name = str(uuid.uuid4()) + '.' + image_ext.split(r'/')[1]
                        upload_url = current_app.config.get("UPLOADED_PHOTOS_DEST")
                        image_path = os.path.join(upload_url, image_name)
                        # 将图片保存到指定路径
                        with open(image_path, 'wb') as f:
                            f.write(image_bytes)
                        size = os.path.getsize(image_path)
                        image_url = photos.url(image_name)
                        # 保存图片上传信息
                        photo = PhotoModel(name=image_name, href=image_url, mime=image_ext, size=size)
                        db.session.add(photo)
                        db.session.commit()
                        # 替换原字符串
                        bug_desc = bug_desc.replace(str(image_data), image_url)

        data = {
            "bug_title": bug_title,
            "space_id": space_id,
            "bug_type": bug_type,
            "bug_level": bug_level,
            "bug_status": bug_status,
            "fix_num": fix_num,
            "hit_run": int(hit_run),
            "bug_design_type": bug_design_type,
            "bug_require_type": bug_require_type,
            "bug_require_stage": bug_require_stage,
            "project_name": project_name,
            "system_name": system_name,
            "programmer": programmer,
            "is_own": is_own,
            "tester": tester,
            "bug_desc": bug_desc,
        }

        DefectModel.query.filter_by(id=_id).update(data)
        db.session.commit()
        if not data:
            return fail_api(message="更新缺陷失败")
        return success_api(message="更新缺陷成功")

    # 删除缺陷
    def delete(self, _id):
        r = DefectModel.query.filter_by(id=_id).delete()
        db.session.commit()
        if not r:
            return fail_api(message="缺陷删除失败")
        return success_api(message="缺陷删除成功")


def defect_deletes():
    ids = request.form.getlist('ids[]')
    for id in ids:
        DefectModel.query.filter_by(id=id).delete()
        db.session.commit()
    return success_api(message="批量删除成功")
