from common import register_api

from .defect import DefectApi, defect_deletes


def register_defect_api(api_bp):
    register_api(DefectApi, 'defects_defect_api', '/defect/info/', pk='_id', app=api_bp)
    register_api(DefectApi, 'defects_defect_del_api', '/defect/info/del/', pk='_id', app=api_bp)

    api_bp.add_url_rule('/defect/info', endpoint='defects_info_del_api', view_func=defect_deletes, methods=['DELETE'])
