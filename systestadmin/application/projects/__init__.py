from common import register_api

from .project import ProjectsView


def register_projects_api(api_bp):
    """
    注册项目相关接口
    """
    register_api(view=ProjectsView, endpoint='projects_project_api', url='/projects/info/', pk='_id', app=api_bp)
