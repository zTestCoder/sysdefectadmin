from flask import request
from flask.views import MethodView
from common.utils.http import table_api

from plugins import db

from models import ProjectsModel


class ProjectsView(MethodView):
    def get(self, _id):
        """
        获取项目列表
        """
        if not _id:
            pages = request.args.get("page", default=1, type=int)
            per_page = request.args.get("limit", default=10, type=int)
            # 查询条件
            filters = []
            project_name = request.args.get("project_name", default='', type=str)
            if project_name:
                filters.append(ProjectsModel.name.like("%" + project_name + "%"))
            # 返回查询列表
            projects = ProjectsModel.query.filter(*filters).paginate(pages, per_page, False)
            return table_api(
                result={"items": [project.to_json() for project in projects.items], "total": projects.total},
                code=0,
            )
        else:
            project = ProjectsModel.query.get(_id)
            return table_api(result=project.to_json(), code=0)

    def post(self):
        """
        创建项目
        """
        project = ProjectsModel(**request.json)
        db.session.add(project)
        db.session.commit()
        return {"project": project.to_json()}
