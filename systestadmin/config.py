import os
import logging

# 主题面板的链接列表配置
SYSTEM_NAME = "云融测试管理"

SYSTEM_PANEL_LINKS = [
    {
        "icon": "layui-icon layui-icon-auz",
        "title": "互金自动化平台",
        "href": "http://12.99.123.18:8080/AutoWeb/index.html"
    },
    {
        "icon": "layui-icon layui-icon-auz",
        "title": "消金自动化平台",
        "href": "http://12.99.86.8:8080/AutoWeb/index.html"
    },
    {
        "icon": "layui-icon layui-icon-auz",
        "title": "运维DevOps系统",
        "href": "http://12.99.102.243:8080"
    },
    {
        "icon": "layui-icon layui-icon-auz",
        "title": "奋进号",
        "href": "http://dev.nbcb.com"
    },
    {
        "icon": "layui-icon layui-icon-auz",
        "title": "知识库",
        "href": "http://12.99.229.1:8181"
    }
]

SECRET_KEY = os.getenv('SECRET_KEY', 'dev key')

# redis 配置
REDIS_HOST = "127.0.0.1"
REDIS_PORT = 6379

# mysql 配置
MYSQL_USERNAME = "root"
MYSQL_PASSWORD = "123a"
MYSQL_HOST = "127.0.0.1"
MYSQL_PORT = 3306
MYSQL_DATABASE = "SysTestAdmin"

""" Sqlalchemy 配置 """
SQLALCHEMY_DATABASE_URI = r'sqlite:///SysTestAdmin.db'
SQLALCHEMY_TRACK_MODIFICATIONS = True
SQLALCHEMY_ECHO = False
SQLALCHEMY_POOL_RECYCLE = 8
# SQLALCHEMY_DATABASE_URI = f"mysql+pymysql://{MYSQL_USERNAME}:{MYSQL_PASSWORD}@{MYSQL_HOST}:{MYSQL_PORT}/{MYSQL_DATABASE}"

LOG_LEVEL = logging.ERROR

# JSON配置
JSON_AS_ASCII = False

# 图片文件存放位置
UPLOADED_PHOTOS_DEST = os.path.join(os.path.dirname(os.path.abspath(__name__)), 'static', 'upload')
UPLOADED_FILES_ALLOW = ['gif', 'jpg', 'png']

# 临时文件存放位置
TEMP_FILE_DEST = os.path.join(os.path.dirname(os.path.abspath(__name__)), 'temp')
