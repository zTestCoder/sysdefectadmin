# SysTestAdmin
![配置](./orther/images/1701420383969525760.svg)

#### 介绍
测试管理平台

#### 软件架构

项目前端：layui，项目后端：flask

项目 Python 环境：>= Python3.7.9，依赖包：requirement.txt

项目目录架构：

```
Sys Test Admin
├─application # 后端应用
│  ├─defect  # 缺陷管理逻辑
│  ├─rights  # 权限处理逻辑
│  ├─system  # 系统登录处理
│  ├─users  # 用户处理逻辑
│  └─view   # 视图部分（路由）
├─common # 通用工具脚本目录
├─models  # 数据模型
├─plugins # 注册插件
├─static  # 静态资源文件
│  ├─admin  
│  │  ├─admin 
│  │  └─component  
│  │     ├─code  
│  │     ├─layui # layui 原本的静态文件
│  │     └─pear  # pear admin layui 的静态文件
│  │        ├─css  # css 样式
│  │        ├─font  # 字体文件
│  │        ├─module  # pear admin layui 集成的插件
│  │        └─pear.js  # 集成插件的文件
│  ├─data   # 数据库初始化数据
│  └─upload # 图片上传存放位置
├─temp # 临时文件存放目录（占位）
├─templates # 静态模板文件
│  ├─admin  # 后台管理模板
│  │  ├─common  # 静态网页公共部分
│  │  ├─defect  # 缺陷管理模板
│  │  ├─file  # 文件管理
│  │  ├─roles # 角色管理
│  │  ├─users # 用户管理
│  │  ├─rights # 权限管理
│  │  └─department # 部门管理
│  ├─errors # 错误信息模板
│  └─index  # 后台页面首页展示
│     ├─login.html # 登录页面
│     ├─welcome.html # 登录后管理台的默认首页
│     └─admin_index.html # 登录后管理台的框架页面
├─migrations  # 迁移文件记录（生成迁移库表数据的临时文件夹）
├─requirement # 项目环境依赖库文件
├─wsgi.ini # uwsgi启动的配置文件
├─config.py # 项目的配置文件
└─main.py # 项目启动入口
```

库表说明：

	cp_user > rt_user_role > rt_role > rt_role_power > rt_power
	
	cp_user：用户表
	rt_user_role：用户与角色的对应关系
	rt_role：角色表
	rt_role_power：角色与菜单的权限关系
	rt_power：系统菜单

#### 安装教程

1.  创建虚拟环境：python3.7 -m venv python379

	激活虚拟环境：python379\Scripts\activate

	安装环境依赖：pip install -r requirement.txt

	下载失败的临时添加国内清华镜像：

	pip install Pillow==8.2.0 -i https://pypi.tuna.tsinghua.edu.cn/simple

	pip install pydantic==1.9.1 -i https://pypi.tuna.tsinghua.edu.cn/simple

	pip install SQLAlchemy==1.4.18 -i https://pypi.tuna.tsinghua.edu.cn/simple

	python -m pip install --upgrade pip -i https://pypi.tuna.tsinghua.edu.cn/simple

    结构：

        \envs

            \python379

            \projects

                \sys-defect-admin

2.  从gitee下载源码至projects目录

	https://gitee.com/zTestCoder/sysdefectadmin.git

3.  数据迁移

	```shell
	# 初始化迁移脚本，生成 migrations 迁移记录文件夹
	flask db init
	# 提交迁移记录
	flask db migrate -m '数据初始化'
	# 生成数据表
	flask db upgrade

	# 生成数据
	flask init-db
	```

	初始化迁移脚本

	![配置](./orther/images/2023-09-04_204852.png)

	生成库表，并插入数据记录

	![配置](./orther/images/mysql_2023-09-05_001919.png)

	启动项目

	![配置](./orther/images/run_server-2023-09-05_000927.png)

4. 本地开发，命令启动

	flask run

5. uwsgi 部署：

	pip install uwsgi

	> 项目启动后，若访问服务失败，则打开Linux系统防火墙5000端口号
    >
	>	sudo firewall-cmd --permanent --add-port=5000/tcp
    >
	>	sudo firewall-cmd --reload
    >

#### 二次开发步骤

1.  修改数据库配置，配置文件：config.py

	（提前安装好数据库）创建数据库实例：SysTestAdmin

	```sql
	CREATE DATABASE SysTestAdmin
    DEFAULT CHARACTER SET = 'utf8mb4';
	```

	注释掉默认的 sqlite，打开MySQL的配置

	![配置](./orther/images/code_20230913142112.png)


#### 说明

1.  本项目使用 Pear Admin Flask 框架进行二次开发（mini分支）
